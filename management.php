<?php

if(!isset($_SESSION["uname"])){
    header('Location: index.php');
}
function letezik($uname){
    return isset(json_decode(file_get_contents("users.json"))->$uname);
} 

function jelszo($uname, $pw){
    return password_verify($pw, json_decode(file_get_contents("users.json"))->$uname->jelszo);
}

function volt($sname){
    foreach(json_decode(file_get_contents("subject.json"),true) as $one){
        if($one["user"] == $_SESSION["uname"] && $one["subject"] == $sname){
            return true;
        }
    }
    return false;
}

function voltFeladat($sname){

}

function newSubject($sname, $tag, $uname){
    $subject = (object)[
        "subject" => $sname,
        "tag" => $tag,
        "user" => $uname
    ];
    $i = 0;
    $json2 = json_decode(file_get_contents("subject.json"),true);
    foreach($json2 as $one){
        ++$i;
    }
    $json = json_decode(file_get_contents("subject.json"));
    $json->$i = $subject;
    file_put_contents('subject.json', json_encode($json,JSON_PRETTY_PRINT));
}


function newTask($subject,$type,$taskName,$priority,$date){
    $i = 0;
    $json2 = json_decode(file_get_contents("task.json"),true);
    foreach($json2 as $one){
        ++$i;
    }
    $task = (object)[
        "subject" => $subject,
        "type" => $type,
        "taskName" => $taskName,
        "priority" => $priority,
        "date" => $date,
        "user" => $_SESSION["uname"],
        "finished" => "false",
        "id" => $i
    ];
    
    $json = json_decode(file_get_contents("task.json"));
    $json->$i = $task;
    file_put_contents('task.json', json_encode($json,JSON_PRETTY_PRINT));
}

function hozzaad($uname, $pw, $email){
    $user = (object)[
        "jelszo" => password_hash($pw, PASSWORD_DEFAULT),
        "email" => $email
    ];
    $json = json_decode(file_get_contents("users.json"));
    $json->$uname = $user;
    file_put_contents('users.json', json_encode($json,JSON_PRETTY_PRINT));
}

?>