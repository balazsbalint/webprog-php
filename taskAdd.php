<?php session_start();
require_once('management.php');
$json =  json_decode(file_get_contents("subject.json"),true);
?>
<?php
if(!isset($_POST["task"]) && !isset($_GET["sent"])){
    header("Location: index.php");
}
if(isset($_GET["sent"])){
    if(!isset($_POST["subjects"])){
        $errorArr[] = "A tárgy választása kötelező! Ha nincs ilyen, akkor vegyél fel egyet a tárgy felvétele oldalon!";
    }
    if(!isset($_POST["type"])){
        $errorArr[] = "A feladat kiválasztása kötelező!";
    }
    if($_POST["taskName"] == ""){
        $errorArr[] = "A feladat nevének megadása kötelező!";
    }
    if($_POST["nap"] == ""){
        $errorArr[] = "A dátum megadása kötelező!";
    }
    if(empty($errorArr)){
        newTask($_POST["subjects"],$_POST["type"],$_POST["taskName"],$_POST["priority"],$_POST["nap"]);
        header("Location: index.php");
    }
}

?>
<form action="taskAdd.php?sent=true" method="post">
    <label for="subjects">Válasszon a felvett tárgyai közül! </label>
    <select name="subjects">
    <option value="" selected disabled hidden>---Válasszon!</option>
    <?php foreach($json as $piece): ?>
        <?php if($piece["user"] == $_SESSION["uname"]) : ?>
            <option value="<?=$piece["subject"] ?>" <?php
                    if( isset($_POST["subjects"]) ){
                        if($piece["subject"] == $_POST["subjects"]){
                               echo "selected";
                        }                
                    }
                ?>>  <?=$piece["subject"] ?></option>
        <?php endif; ?>
    <?php endforeach; ?>
    </select>
    <br><br>
    <label >Adja meg a feladat típusát:</label>
            <select name="type" id="type">
                <option value="" selected disabled hidden>---Válasszon!</option>
                <option value="Beadandó" <?php 
                    if(isset($_POST['type'])){
                        if($_POST['type'] == 'Beadandó'){
                            echo 'selected';
                        }
                    }
            ?>>Beadandó</option>
                <option value="Beszámoló" <?php 
                    if(isset($_POST['type'])){
                        if($_POST['type'] == 'Beszámoló'){
                            echo 'selected';
                        }
                    }
            ?>>Beszámoló</option>
                <option value="ZH" <?php 
                    if(isset($_POST['type'])){
                        if($_POST['type'] == 'ZH'){
                            echo 'selected';
                        }
                    }
            ?>>ZH</option>
                <option value="Vizsga" <?php 
                    if(isset($_POST['type'])){
                        if($_POST['type'] == 'Vizsga'){
                            echo 'selected';
                        }
                    }
            ?>>Vizsga</option>
                <option value="Egyéb" <?php 
                    if(isset($_POST['type'])){
                        if($_POST['type'] == 'Egyéb'){
                            echo 'selected';
                        }
                    }
            ?>>Egyéb</option>
            </select>
            
    <br>
    <label >A feladat neve: </label> <input name="taskName" value="<?= $_POST["taskName"] ?? ""?>"><br><br>
    <label for="priority">Válasszon prioritást! </label>
    <select name="priority" >
        <option value="1" <?php 
                if(isset($_POST['priority'])){
                    if($_POST['priority'] == '1'){
                        echo 'checked';
                    }
                }
            ?> >1</option>
        <option value="2" <?php 
                if(isset($_POST['priority'])){
                    if($_POST['priority'] == '2'){
                        echo 'selected';
                    }
                }
            ?>>2</option>
        <option value="3" <?php 
                if(isset($_POST['priority'])){
                    if($_POST['priority'] == '3'){
                        echo 'selected';
                    }
                }
            ?>>3</option>
        <option value="4" <?php 
                if(isset($_POST['priority'])){
                    if($_POST['priority'] == '4'){
                        echo 'selected';
                    }
                }
            ?>>4</option>
        <option value="5" <?php 
                if(isset($_POST['priority'])){
                    if($_POST['priority'] == '5'){
                        echo 'selected';
                    }
                }
            ?>>5</option>
    </select>
    <br><br>
    <label >Adja meg a határidőt!</label><br>
    <input type="datetime-local" name="nap" placeholder="YYYY-MM-DD" value="<?=$_POST["nap"] ?? ""?>">
    <br><br>

    <input name="send" type="submit" value="Feladat felvétele">
</form>
<?php if (!empty($errorArr)) : ?>
      <div role="alert">
          <?php for($i = 0; $i< count($errorArr); $i++){
              echo "<b>";
              echo $errorArr[$i] ;
              echo "</b>";
              echo "<br>";
          }
         ?>
      </div>
<?php endif; ?>
<form action="index.php" method="post">
    <input name="task" type="submit" value="Vissza a főoldalra!">
</form>
