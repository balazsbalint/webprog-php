<?php
session_start();
require_once("management.php");
if(isset($_POST['subjectSubmit'])){
    if($_POST["subjectName"] == ""){
        $errors[] = "A tárgy nevének megadása kötelező!";
    }

    if($_POST["subjectTag"] == ""){
        $errors[] = "A tárgy rövidítésének megadása kötelező!";
    }
    
    if(! (strlen($_POST['subjectName'])> strlen($_POST['subjectTag'])) ){
        $errors[] = "A tárgy rövidítésének hossza legyen rövidebb, mint a neve!";
    }

    if(volt($_POST['subjectName'])){
        $errors[] = "Ezt a tárgyat már felvetted a nevedhez";
    }

    if(empty($errors)){
        newSubject($_POST["subjectName"],$_POST["subjectTag"],$_SESSION['uname']);
        header("Location: index.php");
    }
}


if(isset($_POST['subject']) || isset($_POST['subjectSubmit'])){
    ?>

   
<form action="" method="post">

    <label >Tantárgy neve</label><br>
    <input type="text" id="subjectName" name="subjectName" value="<?=$_POST["subjectName"] ?? "" ?>" required ><br>
    <label >Tantárgy rövidítése</label><br>
    <input type="text" id="subjectTag" name="subjectTag" value="<?=$_POST["subjectTag"] ?? "" ?>"  required><br><br>
    <button type="submit" name="subjectSubmit">Tantárgy felvétele</button>

</form>

<?php if (!empty($errors)) : ?>
      <div role="alert">
          <?php for($i = 0; $i< count($errors); $i++){
              echo "<b>";
              echo $errors[$i] ;
              echo "</b>";
              echo "<br>";
          }

         ?>
      </div>
<?php endif; ?>

<?php
}else{
    header("Location: index.php");
}

?>
<form action="index.php" method="post">
    <input name="task" type="submit" value="Vissza a főoldalra!">
</form>
