<?php session_start(); ?>
<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Feladatnyilvántartó</title>
</head>
<style>
table,td {
    border: 3px solid black;
    margin: 20px;
    font-size: 120%;
    font-weight: bold;
}
table{
    background-color: yellow; 
}
.expired{
    background-color: red;
}
.prior{
    font-size: 140%;
    border: 8px solid black;
}
.days{
    background-color: green;

}
.finished{
    background-color: grey;
    opacity: 0.5;
}
</style>
<body>
    <?php
        $bejelentkezeve = isset($_SESSION["uname"]);
    ?>

    <?php if($bejelentkezeve): ?>
        <?php require("management.php"); ?>
        <?php require("loggedin.php"); ?>

        <br><br>

        <form action="logout.php" method="post">
            <input name="logout" type="submit" value="Kijelentkezés">
        </form>
    <?php else: ?>
        Bejelentkezés<br>
        <form action="login.php" method="post">
            Felhasználónév: <input name="uname"> <br>
            Jelszó: <input type="password" name="pw"> <br>
            <input name="login" type="submit" value="Bejelentkezés">
        </form>
        <?php if(isset($_GET["hiba"]) && $_GET["hiba"] == "wrong_password"): ?>
            Ehhez a felhasználóhoz más jelszó tartozik
        <?php elseif(isset($_GET["hiba"]) && $_GET["hiba"] == "no_user"): ?>
            Hibás, vagy nem létezik a megadott felhasználó.
        <?php endif ?>

        <br><br>

        Regisztráció<br>
        <form action="register.php" method="post">
            Teljes név: <input name="fullname"> <br>
            E-mail cím: <input name="email"> <br>
            Jelszó: <input type="password" name="pw"> <br>
            <input name="register" type="submit" value="Regisztrál">
        </form>

        <?php if(isset($_GET["hiba"])): ?>
            <?php if($_GET["hiba"] == "register_letezik"): ?>
                Ilyen felhazsnáló már létezik.
            <?php elseif($_GET["hiba"] == "register_pw"): ?>
                A jelszó legalább 8 karakter.
            <?php elseif($_GET["hiba"] == "register_email"): ?>
                Az e-mail cím formátuma nem jó.
            <?php elseif($_GET["hiba"] == "missing_name"): ?>
                Név megadása kötelező
            <?php elseif($_GET["hiba"] == "missing_email"): ?>
                Email megadása kötelező
            <?php elseif($_GET["hiba"] == "missing_pw"): ?>
                Jelszó megadása kötelező    
            <?php endif ?>
        <?php endif ?>
    <?php endif ?>
</body>
</html>