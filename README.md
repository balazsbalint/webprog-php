Webprogramozás beadandó

Kötelező elvárások:

- [x] Bejelentkezés/Kijelentkezés: Van lehetőség az oldalra regisztrálni, a regisztrált felhasználók be tudnak lépni.
- [x] Tantárgy felvétel: Egy külön oldalon lehetőség van tantárgyakat felvenni.
- [x] Feladat felvétel: Egy külön oldalon lehetőség van feladatot felvenni. A típus legördülő listából válaszható ki. 
- [x] Főoldal/Kártya nézet: A főoldalon kártyák formájában látszódnak a felvett feladatok. A kártyákon a feladat minden adata látszik.

További elvárások: 

- [x] Bejelentkezés/Kijelentkezés: Megfelelően ellenőrzi a regisztrációkor a bemenetet, a hibákról üzenetbenértesíti a felhasználókat.
- [x] Tantárgy felvétel: Megfelelően ellenőrzi a bemenetet, a hibákról üzenetben értesíti a felhasználókat.
- [x] Feladat felvétel: A tantárgy kiválasztásánál legördülő listából lehet tárgyat választani.
- [x] Feladat felvétel: Mindenki csak a saját tárgyait látja.
- [x] Feladat felvétel: Megfelelően ellenőrzi a bemenetet, a hibákról üzenetben értesíti a felhasználókat.
- [x] Főoldal/Kártya nézet: Csak a felhasználó saját kártyái látszanak.
- [x] Főoldal/Kártya nézet: A legmagasabb prioritású, illetve a 3 napon belüli határidejű kártyák ki vannak emelve. A lejárt határidejű feladatok kártyája még jobban ki van emelve.
- [x] Főoldal/Kártya nézet: A kártyák rendezhetőek prioritás és határidő szerint.
- [x] Főoldal: Van lehetőség késznek jelölni egy feladatot.
- [x] Főoldal: Kártya nézetben a késznek jelölt feladatokon ne legyen kiemelés, legyen félig átlátszó. 
- [x] Főoldal: A késznek jelölést AJAX/Fetch technológiával oldja meg.
- [x] A regisztráció és a bejelentkezés kivételével a többi oldalhoz csak bejelentkezve lehet hozzáférni.
- [x] Az űrlapok szöveges/szám/dátum mezői hiba esetén megőrzik az állapotukat.
- [x] Az űrlapok legördülő listái hiba esetén megőrzik az állapotukat.
