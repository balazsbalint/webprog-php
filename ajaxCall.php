<?php 
session_start();
if(!isset($_POST["id"])){
    header("Location: index.php");
}
$json = json_decode(file_get_contents("task.json"),true);
foreach($json as $iterate){
    if($iterate["user"] == $_SESSION["uname"] && $iterate["finished"] == "false"){
        if($iterate["id"] == intval($_POST["id"])){
            $json2 = json_decode(file_get_contents("task.json"));
            foreach($json2 as $j){
                if($j->id == $iterate["id"]){
                    $j->finished = "true";
                }
            }
            file_put_contents('task.json',json_encode($json2,JSON_PRETTY_PRINT));
        }
    }
}
?>
