<?php 
if(!isset($_SESSION["uname"])){
    header("Location: index.php");
}

echo "<b>Szia " ;
echo $_SESSION["uname"];
echo " !</b><br><br>";

$json = json_decode(file_get_contents("task.json"),true);
?>
<form action="subjectAdd.php" method="post">
    <input name="subject" type="submit" value="Tantárgy felvétele">
</form>
<br><br>
<form action="taskAdd.php" method="post">
    <input name="task" type="submit" value="Feladat felvétele">
</form>
<br><br>
<b class="finished"style="font-size:120%;" >A befejezett feladatok így jelennek meg.</b><br><br>
<b style="background-color: yellow; font-size:120%;">Az átlagos feladatot sárga háttérszín jelzi.</b><br><br>
<b class="days" style="font-size:120%;">A három napon belül lejáró feladatokat zöld háttér szín jelöli.</b><br><br>
<b class="expired" style="font-size:120%;">A lejárt feladatokat piros háttér szín jelöli.</b><br><br>
<b class="prior" style="font-size:120%;">Az 5-ös prioritású feladatokat vastag keret és nagyobb betűméret jelöli.</b><br><br>
<form action="index.php?order=priority1" method="post">
    <input name="task" type="submit" value="Rendezés prioritás szerint növekvően">
</form>
<form action="index.php?order=priority2" method="post">
    <input name="task" type="submit" value="Rendezés prioritás szerint csökkenően">
</form>
<br>
<form action="index.php?order=date1" method="post">
    <input name="task" type="submit" value="Rendezés dátum és idő szerint (legújabb idő elöl)">
</form>
<form action="index.php?order=date2" method="post">
    <input name="task" type="submit" value="Rendezés dátum és idő szerint (legrégebbi idő elöl)">
</form>
<?php
if(isset($_GET["order"])){
    if($_GET["order"] == "priority1"){
        $prior = array();
        foreach ($json as $key => $row)
        {
            $prior[$key] = $row['priority'];
        }
        array_multisort($prior, SORT_ASC, $json);
    }
    if($_GET["order"] == "priority2"){
        $prior = array();
        foreach ($json as $key => $row)
        {
            $prior[$key] = $row['priority'];
        }
        array_multisort($prior, SORT_DESC, $json);
    }
    if($_GET["order"] == "date1"){
        $date = array();
        foreach ($json as $key => $row)
        {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $json);

    }
    if($_GET["order"] == "date2"){
        $date = array();
        foreach ($json as $key => $row)
        {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_ASC, $json);

    }
}
?>
<?php foreach($json as $one): ?>
    <?php if($one["user"] == $_SESSION["uname"]): ?>
        <table class="<?php 
    $d = date('Y-m-d\TH:i');
    $d1 = strtotime($d);
    $d2 = strtotime($one['date']);
    $days = ($d2-$d1) /86400;
    if($one["finished"] == "true"){
        echo "finished";
        echo " ";
    }else{
        if($days < 3 && $days >0){
            echo "days";
            echo " ";
        }
        if($one['priority'] =="5"){
            echo "prior";
            echo " ";
        }
        if($days < 0){
            echo "expired";
            echo " ";
        }
    }
    
?>">
        <tr><td>A feladat neve: </td><td><?=$one["taskName"] ?> </td></tr>
        <tr><td>A feladathoz tartozó tárgy: </td><td><?=$one["subject"] ?> </td></tr>
        <tr><td>A feladat típusa: </td><td><?=$one["type"] ?> </td></tr>
        <tr><td>A feladat prioritása: </td><td><?=$one["priority"] ?> </td></tr>
        <tr><td>A feladat lejárati ideje: </td><td><?=$one["date"] ?>  </td></tr>
        <?php if($one["finished"] =="false"): ?>
        <tr>
        <td>A feladat befejezése: </td>
        <td>
            <button type="button" id="<?= $one["id"]?>" class="button">Befejezés</button>
        </td>
        </tr>
        <?php elseif($one["finished"] == "true"): ?>
        <tr>
            <td>A feladat be van fejezve!</td> 
        </tr>
        <?php endif; ?>
        </table>
        <br>
    <?php endif; ?> 
<?php endforeach; ?>
<script>
    let finish = document.getElementsByClassName("button");
    for(let one of finish){
        one.addEventListener('click',()=>{
            let xhr = new XMLHttpRequest();
            xhr.open('POST', 'ajaxCall.php',true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send('id='+ one.id);
            xhr.onreadystatechange = function() { 
                 if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                        location.reload();
                 }
            }
        });
    }
</script>