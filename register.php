<?php
session_start();
require_once("management.php");

if(isset($_POST['register'])){

    if(!letezik($_POST['fullname'])){
        if($_POST['fullname'] == ""){
            header("Location: index.php?hiba=missing_name");
        }
        if($_POST['email'] == ""){
            header("Location: index.php?hiba=missing_email");
        }
        if($_POST['pw'] == ""){
            header("Location: index.php?hiba=missing_pw");
        }
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            if(strlen($_POST['pw']) >= 8){ 
                hozzaad($_POST['fullname'],$_POST['pw'],$_POST['email']);
                $_SESSION['uname'] = $_POST['fullname'];
                $_SESSION['uemail'] = $_POST['email'];+
                header("Location: index.php");
            }else{
                header("Location: index.php?hiba=register_pw");
            }
        }else{
            header("Location: index.php?hiba=register_email");
        }

    }else{
        header("Location: index.php?hiba=register_letezik");
    }
    
    
}else{
    header("Location: index.php");
} 


?>